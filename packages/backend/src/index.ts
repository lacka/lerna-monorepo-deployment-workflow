import Koa, { Context } from "koa";
import Router from "@koa/router";

const PORT = 1337;
const app = new Koa();

const router = new Router();

router.get("Home", "/", (ctx: Context) => {
  ctx.status = 200;
  ctx.body = "Hello Backend!";
});

router.get("Status", "/status", (ctx: Context) => {
  ctx.status = 200;
  ctx.body = { status: "OK", date: new Date().toJSON() };
});

app.use(router.routes()).use(router.allowedMethods());

app.listen(PORT, "0.0.0.0", () => {
  console.log(`Backend started on port: ${PORT}`);
});
